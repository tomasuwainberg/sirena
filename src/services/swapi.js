import buildApi from './api';
import config from './config';

const buildSwapi = () => {
  const { get } = buildApi(config.swapi);

  return {
    getCharacter: characterId =>
      get(`people/${characterId}/`).then(res => res.data),
    getCharacters: ({ url = null, name = '' } = {}) =>
      get(url || 'people/', name ? { search: name } : null).then(res => res.data),
    getFilm: filmId => get(`films/${filmId}/`).then(res => res.data),
    getFilms: ({ page = '', title = '' } = {}) =>
      get('films/', { search: title, page }).then(res => res.data),
  };
};

export default buildSwapi;
