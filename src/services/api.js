import { create } from 'apisauce';

const buildApi = config => {
  const api = create(config);

  return api;
};

export default buildApi;
