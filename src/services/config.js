const config = {
  swapi: {
    baseURL: 'https://swapi.co/api/'
  }
};

export default config;
