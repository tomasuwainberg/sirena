import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { setLoading, setFilms, setCharacters, setCurrentCharacter, setCurrentFilm } from '../../state/actions';
import swapi from '../../services/index';
import constants from '../../config/constants';
import messages from '../../config/messages';

import CharactersPanel from '../../components/CharactersPanel';
import FilmsPanel from '../../components/FilmsPanel';
import Spinner from '../../components/Spinner';
import Menu from '../../components/Menu';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

const App = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [section, setSection] = useState(constants.sections.characters);
  const [filterTitle, setFilterTitle] = useState('');
  const [filterName, setFilterName] = useState('');
  const [isFetching, setIsFetching] = useState(false);

  const onScrollCharactersList = event => {
    if (event.target.offsetHeight + event.target.scrollTop !== event.target.scrollHeight || isFetching) return;

    setIsFetching(true);
  }

  const {
    loading,
    characters,
    films,
    currentCharacter,
    currentFilm
  } = useSelector(state => state.app);

  const getFilms = () => {
    dispatch(setLoading(true))

    swapi.getFilms().then(serviceFilms => {
      dispatch(setFilms(serviceFilms));
      dispatch(setLoading(false));
    }).catch(err => err);
  };

  const getCharacters = (overrideData = false) => {
    dispatch(setLoading(true))
    let url = characters.nextUrl;

    if (overrideData) {
      url = null;
    }

    swapi.getCharacters({ url, name: filterName }).then(serviceCharacters => {
      dispatch(setCharacters(serviceCharacters, overrideData));
      dispatch(setLoading(false));
      setIsFetching(false);
    }).catch(err => err);
  };

  useEffect(() => {
    getCharacters();
    getFilms();
  }, []);

  useEffect(() => {
    if (!isFetching) return;

    if (characters.nextUrl) {
      getCharacters();
    } else {
      setIsFetching(false);
    }
  }, [isFetching]);

  useEffect(() => {
    if (section === constants.sections.characters) {
      getCharacters();
    } else {
      getFilms();
    }

    setFilterName('');
    setFilterTitle('');
  }, [section]);

  useEffect(() => {
    getCharacters(true);
  }, [filterName]);

  const setCharactersSection = () => {
    setSection(constants.sections.characters);
    dispatch(setCurrentCharacter(null));
  };

  const setFilmsSection = () => {
    setFilterTitle('');
    setSection(constants.sections.films);
    dispatch(setCurrentFilm(null));
  };

  const dispacthCurrentItem = url => {
    if (section === constants.sections.characters) {
      dispatch(setCurrentCharacter(url));
    } else {
      dispatch(setCurrentFilm(url));
    }
  };

  const getCurrentCharacter = () => {
    if (characters && films && characters.list) {
      const character = characters.list.find(c => c.url === currentCharacter);
      let newCharacter = null;

      if (character) {
        newCharacter = {...character}
        newCharacter.films = character.films.map(f => films.find(film => film.url === f));
      }

      return newCharacter;
    }
    
    return null;
  };

  const film = films ? films.find(f => f.url === currentFilm) : null;

  const filterFilms = films ? films.filter(f => f.title.toUpperCase().includes(filterTitle.toUpperCase())) : [];

  const changeFilterTitle = event => setFilterTitle(event.target.value);

  const changeFilterName = event => setFilterName(event.target.value);

  const onClickFilm = filmUrl => {
    setFilmsSection();
    dispatch(setCurrentFilm(filmUrl));
  };

  const menus = [
    {
      disabled: section === constants.sections.characters,
      onClick: setCharactersSection,
      selected: section === constants.sections.characters,
      title: messages.characters,
    },
    {
      disabled: section === constants.sections.films,
      onClick: setFilmsSection,
      selected: section === constants.sections.films,
      title: messages.films,
    },
  ];

  return (
    <>
      {loading && <Spinner />}
      <Container fixed className={classes.root}>
        <Menu options={menus} />
        {(!loading || characters || films) && (
          <>
            {section === constants.sections.characters && characters && (
              <CharactersPanel
                characters={characters.list}
                currentCharacter={getCurrentCharacter()}
                onChangeSearch={changeFilterName}
                onClickCharacter={dispacthCurrentItem}
                onClickFilm={onClickFilm}
                onScrollList={onScrollCharactersList}
                title={messages.characters}
              />)}
            {section === constants.sections.films && films && (<FilmsPanel currentFilm={film} films={filterFilms} onChangeSearch={changeFilterTitle} onClickFilm={dispacthCurrentItem} title={messages.films} />)}
          </>
        )}
      </Container>
    </>
  );
};

export default App;
