import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, CircularProgress } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {},
  spinnerContainer: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '1000'
  }
}));

const Spinner = () => {
  const classes = useStyles();

  return (
    <Container fixed className={classes.root}>
      <div className={classes.spinnerContainer}>
        <CircularProgress />
      </div>
    </Container>
  );
};

export default Spinner;
