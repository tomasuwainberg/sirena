import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { Paper, List, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import FilmCard from '../FilmCard';
import SearchBar from '../SearchBar';

const useStyles = makeStyles(theme => ({
  paper: {
    marginRight: theme.spacing(2)
  },
  list: {
    width: '100%',
    maxWidth: 500,
    minWidth: 360,
    maxHeight: 300,
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto'
  },
  listItemSelected: {
    background: theme.palette.action.active
  },
  card: {
    width: 300,
    maxWidth: 300
  }
}));

const FilmsPanel = ({
  currentFilm,
  films,
  onChangeSearch,
  onClickFilm,
  title
}) => {
  const classes = useStyles();

  return (
    <>
      <Paper className={classes.paper}>
        <SearchBar title={title} onChange={onChangeSearch} />
        <List component='nav' className={classes.list}>
          {films.map(f => (
            <ListItem
              className={
                currentFilm && currentFilm.url === f.url
                  ? classes.listItemSelected
                  : null
              }
              button
              key={uuidv4()}
              onClick={() => onClickFilm(f.url)}
              disabled={currentFilm && currentFilm.url === f.url}
            >
              <ListItemText primary={f.title} />
            </ListItem>
          ))}
        </List>
      </Paper>
      {currentFilm ? (
        <FilmCard
          classesRoot={classes.card}
          director={currentFilm.director}
          producer={currentFilm.producer}
          releaseDate={currentFilm.release_date}
          title={currentFilm.title}
        />
      ) : (
        <div className={classes.card} />
      )}
    </>
  );
};

FilmsPanel.propTypes = {
  currentFilm: PropTypes.shape({
    director: PropTypes.string.isRequired,
    producer: PropTypes.string.isRequired,
    release_date: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }),
  films: PropTypes.arrayOf(
    PropTypes.shape({
      director: PropTypes.string.isRequired,
      producer: PropTypes.string.isRequired,
      release_date: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })
  ).isRequired,
  onChangeSearch: PropTypes.func.isRequired,
  onClickFilm: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

FilmsPanel.defaultProps = {
  currentFilm: null
};

export default FilmsPanel;
