import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { Paper, List, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import CharacterCard from '../CharacterCard';
import SearchBar from '../SearchBar';

const useStyles = makeStyles(theme => ({
  paper: {
    marginRight: theme.spacing(2)
  },
  list: {
    width: '100%',
    maxWidth: 500,
    minWidth: 360,
    maxHeight: 300,
    backgroundColor: theme.palette.background.paper,
    overflow: 'auto'
  },
  listItemSelected: {
    background: theme.palette.action.active
  },
  card: {
    width: 300,
    maxWidth: 300
  }
}));

const CharactersPanel = ({
  characters,
  currentCharacter,
  onChangeSearch,
  onClickCharacter,
  onClickFilm,
  onScrollList,
  title
}) => {
  const classes = useStyles();

  return (
    <>
      <Paper className={classes.paper}>
        <SearchBar title={title} onChange={onChangeSearch} />
        <List onScroll={onScrollList} component='nav' className={classes.list}>
          {characters.map(c => (
            <ListItem
              className={
                currentCharacter && currentCharacter.url === c.url
                  ? classes.listItemSelected
                  : null
              }
              button
              key={uuidv4()}
              onClick={() => onClickCharacter(c.url)}
              disabled={currentCharacter && currentCharacter.url === c.url}
            >
              <ListItemText primary={c.name} />
            </ListItem>
          ))}
        </List>
      </Paper>
      {currentCharacter ? (
        <CharacterCard
          classesRoot={classes.card}
          films={currentCharacter.films}
          height={currentCharacter.height}
          mass={currentCharacter.mass}
          name={currentCharacter.name}
          onClickFilm={onClickFilm}
        />
      ) : (
        <div className={classes.card} />
      )}
    </>
  );
};

CharactersPanel.propTypes = {
  characters: PropTypes.arrayOf(
    PropTypes.shape({
      films: PropTypes.arrayOf(PropTypes.string).isRequired,
      height: PropTypes.string.isRequired,
      mass: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired
    })
  ).isRequired,
  currentCharacter: PropTypes.shape({
    films: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired
      })
    ).isRequired,
    height: PropTypes.string.isRequired,
    mass: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired
  }),
  onClickCharacter: PropTypes.func.isRequired,
  onChangeSearch: PropTypes.func.isRequired,
  onClickFilm: PropTypes.func.isRequired,
  onScrollList: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

CharactersPanel.defaultProps = {
  currentCharacter: null
};

export default CharactersPanel;
