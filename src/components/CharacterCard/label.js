const label = {
  eyeColor: 'Color de ojos: ',
  height: 'Altura: ',
  heightSufix: ' cm',
  mass: 'Peso: ',
  massSufix: ' kg',
  filmsTitle: 'Películas en las que apareció:'
};

export default label;
