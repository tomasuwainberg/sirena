import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { Card, CardContent, Divider, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import label from './label';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  cardContent: {
    width: '100%'
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  filmsContainer: {
    marginTop: theme.spacing(2)
  },
  film: {
    textDecoration: 'underline',
    color: 'blue',
    cursor: 'pointer'
  }
}));

const CharacterCard = ({
  classesRoot,
  films,
  height,
  mass,
  name,
  onClickFilm
}) => {
  const classes = useStyles();

  return (
    <Card className={`${classes.root} ${classesRoot}`}>
      <CardContent className={classes.cardContent}>
        <Typography variant='h5' component='h2'>
          {name}
        </Typography>
        <Divider className={classes.divider} />
        <Typography variant='body2' component='p'>
          <strong>{label.height}</strong>
          {height}
          {label.heightSufix}
        </Typography>
        <Typography variant='body2' component='p'>
          <strong>{label.mass}</strong>
          {mass}
          {label.massSufix}
        </Typography>
        <Divider className={classes.divider} />
        <Typography variant='body2' component='p'>
          {label.filmsTitle}
        </Typography>
        <div className={classes.filmsContainer}>
          {films.map(f => (
            <Typography
              className={classes.film}
              key={uuidv4()}
              onClick={() => onClickFilm(f.url)}
            >
              {f.title}
            </Typography>
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

CharacterCard.propTypes = {
  classesRoot: PropTypes.string.isRequired,
  films: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired
    })
  ).isRequired,
  height: PropTypes.string.isRequired,
  mass: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onClickFilm: PropTypes.func.isRequired
};

export default CharacterCard;
