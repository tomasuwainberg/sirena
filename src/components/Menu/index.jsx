import React from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, MenuList, MenuItem } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  paper: {
    marginRight: theme.spacing(2),
    height: 100
  },
  menuList: {
    height: 100
  },
  menuSelected: {
    background: theme.palette.action.active
  }
}));

const Menu = ({ options }) => {
  const classes = useStyles();

  return (
    <Paper className={classes.paper}>
      <MenuList className={classes.menuList}>
        {options.map(o => (
          <MenuItem
            className={o.selected ? classes.menuSelected : null}
            key={uuidv4()}
            onClick={o.onClick}
            disabled={o.disabled}
          >
            {o.title}
          </MenuItem>
        ))}
      </MenuList>
    </Paper>
  );
};

Menu.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      disabled: PropTypes.bool.isRequired,
      onClick: PropTypes.func.isRequired,
      selected: PropTypes.bool.isRequired,
      title: PropTypes.string.isRequired
    })
  ).isRequired
};

export default Menu;
