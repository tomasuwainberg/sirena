const label = {
  director: 'Director: ',
  producer: 'Productor: ',
  releaseDate: 'Fecha de estreno: '
};

export default label;
