import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardContent, Divider, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import label from './label';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  cardContent: {
    width: '100%'
  },
  divider: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));

const FilmCard = ({ classesRoot, director, producer, releaseDate, title }) => {
  const classes = useStyles();

  return (
    <Card className={`${classes.root} ${classesRoot}`}>
      <CardContent className={classes.cardContent}>
        <Typography variant='h5' component='h2'>
          {title}
        </Typography>
        <Divider className={classes.divider} />
        <Typography variant='body2' component='p'>
          <strong>{label.director}</strong>
          {director}
        </Typography>
        <Typography variant='body2' component='p'>
          <strong>{label.producer}</strong>
          {producer}
        </Typography>
        <Typography variant='body2' component='p'>
          <strong>{label.releaseDate}</strong>
          {releaseDate}
        </Typography>
      </CardContent>
    </Card>
  );
};

FilmCard.propTypes = {
  classesRoot: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  producer: PropTypes.string.isRequired,
  releaseDate: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default FilmCard;
