const messages = {
  helloWorld: 'Hello world!',
  characters: 'Personajes',
  films: 'Películas',
  searchPlaceHolder: 'Buscar...'
};

export default messages;
