// Actions

export const SET_LOADING = 'SET_LOADING';
export const SET_FILMS = 'SET_FILMS';
export const SET_CHARACTERS = 'SET_CHARACTERS';
export const SET_CURRENT_FILM = 'SET_CURRENT_FILM';
export const SET_CURRENT_CHARACTER = 'SET_CURRENT_CHARACTER';

// Creators

export const setLoading = value => ({ type: SET_LOADING, payload: value });

export const setFilms = films => ({ type: SET_FILMS, payload: films });

export const setCharacters = (characters, overrideData = false) => ({
  type: SET_CHARACTERS,
  payload: {
    characters,
    overrideData,
  },
});

export const setCurrentFilm = currentFilm => ({
  type: SET_CURRENT_FILM,
  payload: currentFilm
});

export const setCurrentCharacter = currentCharacter => ({
  type: SET_CURRENT_CHARACTER,
  payload: currentCharacter
});
