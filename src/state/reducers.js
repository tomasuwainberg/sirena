import { combineReducers } from 'redux';

import {
  SET_LOADING,
  SET_FILMS,
  SET_CHARACTERS,
  SET_CURRENT_FILM,
  SET_CURRENT_CHARACTER
} from './actions';

const initialState = {
  loading: false,
  films: [],
  characters: {
    nextUrl: null,
    list: [],
  },
  currentFilm: null,
  currentCharacter: null
};

const app = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case SET_FILMS:
      return {
        ...state,
        films: action.payload.results
      };
    case SET_CHARACTERS:
      return {
        ...state,
        characters: {
          nextUrl: action.payload.characters.next,
          list: state.characters.list && !action.payload.overrideData ? [...state.characters.list, ...action.payload.characters.results] : action.payload.characters.results,
        }
      };
    case SET_CURRENT_FILM:
      return {
        ...state,
        currentFilm: action.payload
      };
    case SET_CURRENT_CHARACTER:
      return {
        ...state,
        currentCharacter: action.payload
      };
    default:
      return state;
  }
};

const reducer = combineReducers({
  app
});

export default reducer;
